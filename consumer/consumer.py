import requests
import json
import alooma_pysdk
import boto3
import os

sqs_client = boto3.client('sqs')


def lambda_handler(event,context):    
    message = event
    message_dict = json.loads(message['Records'][0]['body'])
    
    #Extract HTTP request url and request body
    campaign_info_endpoint = message_dict['endpoint']
    endpoint_username = os.environ['username']
    endpoint_password = os.environ['password']

    # # Get campaign info from Eloqua
    campaign_response = requests.get(url=campaign_info_endpoint, auth=(endpoint_username, endpoint_password))

    # # Check response is valid
    assert (campaign_response.status_code == 200), "Bad Campaign Request!"

    campaign_response_dict = campaign_response.json() #Format the JSON response into a dict
    
    # Alooma endpoint data
    INPUT_TOKEN = os.environ['alooma_pythonsdk_inputtoken']
    label =  os.environ['alooma_label']
    
    # Post to Alooma
    alooma_sdk = alooma_pysdk.PythonSDK(INPUT_TOKEN, event_type = label) #Open connection with Alooma
    alooma_sdk.report(campaign_response_dict) #Post API response to Alooma
    alooma_pysdk.terminate() #Terminate connection with Alooma 