import requests
import json
import alooma_pysdk
import boto3

sqs_client = boto3.client('sqs')
s3_client = boto3.resource('s3')


# Reads json files from S3 and returns a Python dictionary
def boto_json_load(bucket, key):
    content_object = s3_client.Object(bucket, key)
    file_content = content_object.get()['Body'].read().decode('utf-8')
    json_content = json.loads(file_content)
    return json_content


def lambda_handler(event,context):    
    # Bucket and key for AWS, should be handled by JSON load function
    bucket = "alooma-dwpp-prod-h0ar4ff8g5"
    key = "EloquaConfig.json"

    config = boto_json_load(bucket, key) #load in JSON object from S3 bucket
    #sqs_url = config['sqs_url']
    #message = sqs_client.receive_message(QueueUrl=sqs_url) 
    message = event
    #print(message)
    #print(type(message))
    #message_dict = json.loads(message['Messages'][0]['Body'])
    
    message_dict = message['Messages'][0]['Body']
    print(message_dict)

    #Extract HTTP request url and request body
    campaign_info_endpoint = message_dict['endpoint']
    endpoint_username = message_dict['username']
    endpoint_password = message_dict['password']

    # Get campaign info from Eloqua
    campaign_response = requests.get(url=campaign_info_endpoint, auth=(endpoint_username, endpoint_password))
    #print(campaign_response)

    # Check response is valid
    assert (campaign_response.status_code == 200),"Bad Campaign Request!"

    campaign_response_dict = campaign_response.json() #Format the JSON response into a dict

    #Extract additional metadata from SNS messasge
    # aws_request_id = message_dict['aws_request_id'] #Currently not being used
    INPUT_TOKEN = message_dict['alooma_input_token']
    # label =  message_dict['alooma_label'] = Maybe pass back in during rendition 2? 
    label = 'nonflattened'


    # Post to Alooma
    alooma_sdk = alooma_pysdk.PythonSDK(INPUT_TOKEN, event_type = label) #Open connection with Alooma
    alooma_sdk.report(campaign_response_dict) #Post API response to Alooma
    alooma_pysdk.terminate() #Terminate connection with Alooma 


    # next, we delete the message from the queue so no one else will process it again
    # sqs_client.delete_message(QueueUrl=sqs_url, ReceiptHandle = message['Messages'][0]['ReceiptHandle'])
