import boto3
import json
import copy
import requests
import os

sqs_client = boto3.client('sqs')

def lambda_handler(event, context):
    getCampaign_endpoint = os.environ['producer_api_endpoint']  #Get the API_Endpoint from the above object
    requestBodyUsername = os.environ['username'] #Get request body from S3 JSON Object
    requestBodyPassword = os.environ['password']
    sqs_url = os.environ['sqs_url']

    #Begin getting the JSON response
    gci = requests.get(url=getCampaign_endpoint, auth=(requestBodyUsername, requestBodyPassword))
    assert (gci.status_code == 200),"Bad Campaign Request!"
    gci_dict = gci.json()
    
    #Only retrieve Campaign id's within the Campaign JSON response
    gci_elements = gci_dict['elements']
    campaign_ids = [x['id']for x in gci_elements]
    chain_campaign = os.environ['consumer_api_endpoint']
    detailed_campaign_request = [str(chain_campaign) + str(id) for id in campaign_ids]

    subrequests = [{"endpoint" : subreq } for subreq in detailed_campaign_request]

	# # Publish to requested topic
    for subreq in subrequests:
	    sqs_client.send_message(QueueUrl = sqs_url, MessageBody = json.dumps(subreq))